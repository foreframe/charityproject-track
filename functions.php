<?php

if  ( ! function_exists('charityproject_setup') ) :
    
    function charityproject_setup() {
        // Let WP handle the title tags
        add_theme_support( 'title-tag' );    
    }

endif;

add_action('after_setup_theme', 'charityproject_setup');


/* --- Register Menus --- */

function register_charityproject_menus () {
    register_nav_menus(
        array(
            'primary' => __('Primary Menu'),
            'footer' => __('Footer Menu'),
            'logo-center' => __('Logo Centered Menu')
        )
    );
}

add_action('init', 'register_charityproject_menus');

/* --- Add Stylesheets --- */

function charityproject_scripts() {
    
    // Enqueue Main Stylesheet
    wp_enqueue_style('charityproject_styles', get_stylesheet_uri() );
    // Enqueue Google Fonts: Raleway
    wp_enqueue_style('charityproject_google_fonts', 'https://fonts.googleapis.com/css?family=Raleway:300,400,400i,700' );
    
}

add_action('wp_enqueue_scripts', 'charityproject_scripts');

/* --- Register Widget Areas --- */

function charityproject_widget_itit() {
    register_sidebar( array(
        'name'          => __( 'Main Sidebar', 'charityproject' ),
        'id'            => 'main-sidebar',
        'description'   => __('Widgets added here will appear in all pages using the Two-Column Template.', 'charityproject' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>'    
    ));
}

add_action( 'widgets_init', 'charityproject_widget_itit' );